tee create_better_tables.txt;

-- indexes
drop table if exists RoomCodes; show warnings;
drop table if exists SectionsWithRooms; show warnings;
drop table if exists RoomFacilities; show warnings;
drop table if exists Teachings; show warnings;
drop table if exists InstructorsWithNames; show warnings;
drop table if exists Instructors; show warnings;
drop table if exists Sections; show warnings;
drop table if exists Schedules; show warnings;
drop table if exists GradeDistributions; show warnings;
drop table if exists SubjectMemberships; show warnings; 
drop table if exists CourseOfferings; show warnings;
drop table if exists Subjects; show warnings;
drop table if exists CoursesWithNames; show warnings;
drop table if exists Courses; show warnings;


select '---------------------------------------------------------------------------------------' as '';
select 'Create Courses' as '';

create table Courses (uuid char(36), 
                course_number decimal(5),
                primary key (uuid), 
                check (course_number > 0)
		    );

insert into Courses select uuid, course_number from OldCourses;

select '---------------------------------------------------------------------------------------' as '';
select 'Create CoursesWithNames' as '';

create table CoursesWithNames (course_uuid char(36), 
                name char(100) NOT NULL,
                primary key (course_uuid),
                foreign key (course_uuid) references Courses(uuid)
		    );

insert into CoursesWithNames select uuid, name from OldCourses where name not like 'null';

create index courseNameIndex on CoursesWithNames(name);

select '---------------------------------------------------------------------------------------' as '';
select 'Create Subjects' as '';

create table Subjects (code char(5), 
                name char(100) NOT NULL, 
                abbreviation char(15) NOT NULL,
                primary key (code)
		    );

insert into Subjects select * from OldSubjects;

select '---------------------------------------------------------------------------------------' as '';
select 'Create CourseOfferings' as '';

create table CourseOfferings (uuid char(36),
                course_uuid char(36),
                term_code decimal(4),
                primary key (uuid), 
                foreign key (course_uuid) references Courses(uuid),
                check (term_code > 0)
            );

insert into CourseOfferings select uuid, course_uuid, term_code from OldCourseOfferings;

create index termCodeIndex on CourseOfferings(term_code);

select '---------------------------------------------------------------------------------------' as '';
select 'Create SubjectMemberships' as '';

create table SubjectMemberships (subject_code char(5), 
                course_offering_uuid char(36),
                primary key (course_offering_uuid, subject_code), 
                foreign key (subject_code) references Subjects(code),
                foreign key (course_offering_uuid) references CourseOfferings(uuid)
		    );

insert into SubjectMemberships select * from OldSubjectMemberships;

select '---------------------------------------------------------------------------------------' as '';
select 'Create GradeDistributions' as '';

create table GradeDistributions (course_offering_uuid char(36),
                section_number decimal(3),
                a_count decimal(4), 
                ab_count decimal(4), 
                b_count decimal(4), 
                bc_count decimal(4), 
                c_count decimal(4), 
                d_count decimal(4), 
                f_count decimal(4), 
                s_count decimal(4), 
                u_count decimal(4), 
                cr_count decimal(4), 
                n_count decimal(4), 
                p_count decimal(4), 
                i_count decimal(4), 
                nw_count decimal(4),
                nr_count decimal(4),
                other_count decimal(4),
                primary key (course_offering_uuid, section_number), 
                foreign key (course_offering_uuid) references CourseOfferings(uuid), 
                check (section_number > 0)
            );

insert into GradeDistributions select * from OldGradeDistributions;

select '---------------------------------------------------------------------------------------' as '';
select 'Create RoomFacilities' as '';

create table RoomFacilities (room_uuid char(36),
                facility_code char(12) NOT NULL, 
                primary key (room_uuid)
            );

insert into RoomFacilities select uuid, facility_code from OldRooms;

select '---------------------------------------------------------------------------------------' as '';
select 'Create RoomCodes' as '';

create table RoomCodes (room_uuid char(36),
                room_code char(6) NOT NULL,
                primary key (room_uuid), 
                foreign key (room_uuid) references RoomFacilities(room_uuid)
            );

insert into RoomCodes select uuid, room_code from OldRooms where room_code not like 'null';

select '---------------------------------------------------------------------------------------' as '';
select 'Create Schedules' as '';

create table Schedules (uuid char(36), 
                start_time decimal(4), 
                end_time decimal(4), 
                mon enum('true', 'false'),
                tues enum('true', 'false'),
                wed enum('true', 'false'),
                thurs enum('true', 'false'),
                fri enum('true', 'false'),
                sat enum('true', 'false'),
                sun enum('true', 'false'), 
                primary key (uuid),
                check (start_time >= -1 AND start_time <= 1439), 
                check (end_time >= -1 AND end_time <= 1439)
		    );

insert into Schedules select * from OldSchedules;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Sections' as '';

create table Sections (uuid char(36), 
			course_offering_uuid char(36), 
			section_type enum ('LAB', 'LEC', 'DIS', 'FLD', 'IND', 'SEM'), 
			section_number decimal(3),
			schedule_uuid char(36), 
            primary key (uuid), 
            foreign key (course_offering_uuid) references CourseOfferings(uuid), 
            foreign key (schedule_uuid) references Schedules(uuid), 
            check (section_number > 0)
		);

insert into Sections select uuid, course_offering_uuid, section_type, section_number, schedule_uuid from OldSections;

create index sectionTypeIndex on Sections(section_type);
create index sectionNumberIndex on Sections(section_number);

select '---------------------------------------------------------------------------------------' as '';
select 'Create SectionsWithRooms' as '';

create table SectionsWithRooms (section_uuid char(36), 
			    room_uuid char(36),
                primary key (section_uuid),
                foreign key (section_uuid) references Sections(uuid),
                foreign key (room_uuid) references RoomFacilities(room_uuid)
		    );

insert into SectionsWithRooms select uuid, room_uuid from OldSections where room_uuid not like 'null';

select '---------------------------------------------------------------------------------------' as '';
select 'Create Instructors' as '';

create table Instructors (id decimal(10),
                primary key (id)
            );

insert into Instructors select id from OldInstructors;

select '---------------------------------------------------------------------------------------' as '';
select 'Create InstructorsWithNames' as '';

create table InstructorsWithNames (instructor_id decimal(10),
                name char(100) NOT NULL, 
                primary key (instructor_id), 
                foreign key (instructor_id) references Instructors(id)
            );

insert into InstructorsWithNames select id, name from OldInstructors where name not like 'null';

create index instructorNameIndex on InstructorsWithNames(name);

select '---------------------------------------------------------------------------------------' as '';
select 'Create Teachings' as '';

create table Teachings (instructor_id decimal(10), 
			section_uuid char(36), 
            primary key (instructor_id, section_uuid),
            foreign key (instructor_id) references Instructors(id),
            foreign key (section_uuid) references Sections(uuid)
		);

insert into Teachings select * from OldTeachings;
