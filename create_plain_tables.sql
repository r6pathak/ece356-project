tee create_plain_tables.txt;

drop table if exists OldCourses;
drop table if exists OldCourseOfferings;
drop table if exists OldGradeDistributions;
drop table if exists OldInstructors;
drop table if exists OldRooms;
drop table if exists OldSchedules;
drop table if exists OldSections;
drop table if exists OldSubjectMemberships;
drop table if exists OldSubjects;
drop table if exists OldTeachings;


select '---------------------------------------------------------------------------------------' as '';
select 'Create OldCourses' as '';

create table OldCourses (uuid char(36), 
               name char(100), 
		     course_number decimal(5)
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/courses.csv' ignore into table OldCourses
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldCourseOfferings' as '';

create table OldCourseOfferings (uuid char(36),
               course_uuid char(36),
		     term_code decimal(4), 
               name char(100)
          );

load data infile '/var/lib/mysql-files/26-Education/UWM/course_offerings.csv' ignore into table OldCourseOfferings
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldGradeDistributions' as '';

create table OldGradeDistributions (course_offering_uuid char(36),
               section_number decimal(3),
               a_count decimal(4), 
               ab_count decimal(4), 
               b_count decimal(4), 
               bc_count decimal(4), 
               c_count decimal(4), 
               d_count decimal(4), 
               f_count decimal(4), 
               s_count decimal(4), 
               u_count decimal(4), 
               cr_count decimal(4), 
               n_count decimal(4), 
               p_count decimal(4), 
               i_count decimal(4), 
               nw_count decimal(4),
               nr_count decimal(4),
               other_count decimal(4)
          );

load data infile '/var/lib/mysql-files/26-Education/UWM/grade_distributions.csv' ignore into table OldGradeDistributions
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldInstructors' as '';

create table OldInstructors (id decimal(10),
               name char(100)
          );

load data infile '/var/lib/mysql-files/26-Education/UWM/instructors.csv' ignore into table OldInstructors
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;
          
select '---------------------------------------------------------------------------------------' as '';
select 'Create OldRooms' as '';

create table OldRooms (uuid char(36),
               facility_code char(12),
               room_code char(6)
          );

load data infile '/var/lib/mysql-files/26-Education/UWM/rooms.csv' ignore into table OldRooms
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldSchedules' as '';

create table OldSchedules (uuid char(36), 
			start_time decimal(4), 
			end_time decimal(4), 
			mon char(5),
			tues char(5),
			wed char(5),
			thurs char(5),
			fri char(5),
			sat char(5),
			sun char(5)
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/schedules.csv' ignore into table OldSchedules
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldSections' as '';

create table OldSections (uuid char(36), 
			course_offering_uuid char(36), 
			section_type char(3), 
			section_number decimal(3),
			room_uuid char(36),
			schedule_uuid char(36)
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/sections.csv' ignore into table OldSections
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldSubjectMemberships' as '';

create table OldSubjectMemberships (subject_code decimal(5), 
			course_offering_uuid char(36)
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/subject_memberships.csv' ignore into table OldSubjectMemberships
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldSubjects' as '';

create table OldSubjects (code char(5), 
               name char(100), 
               abbreviation char(15)
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/subjects.csv' ignore into table OldSubjects
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create OldTeachings' as '';

create table OldTeachings (instructor_id decimal(10), 
			section_uuid char(36)		
		);

load data infile '/var/lib/mysql-files/26-Education/UWM/teachings.csv' ignore into table OldTeachings
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;


notee