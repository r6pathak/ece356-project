import mysql.connector
import uuid
import getpass

def get_mydb():
    # This function initializes the sql connection
    database = str(input("Enter the database name: "))
    user = str(input("Enter the username: "))
    password = str(getpass.getpass('Enter the password:'))
    mydb = mysql.connector.connect(
    host="marmoset04.shoshin.uwaterloo.ca",
    database=database,
    user=user,
    password=password
    )
    return mydb

def prompt_user():
    while(True):
        # This is the main menu, prompt user for choice
        print("1. Insert")
        print("2. Query")
        print("3. Exit")
        choice = int(input("Choose an operation to perform: "))
        # Call the appropriate function
        if(choice==1):
            prompt_user_insert()
        elif(choice==2):
            prompt_user_query()
        elif(choice==3):
            print("Exiting... ")
            break
        else:
            raise AssertionError("Invalid choice")

def prompt_user_insert():
    while(True):
        # Prompt user for choice 
        print("1. Course")
        print("2. Subject")
        print("3. Course Offering")
        print("4. Room")
        print("5. Instructor")
        print("6. Sections")
        print("7. Go Back")
        choice = int(input("Choose what to insert: "))

        if(choice==1):
            # Input values for Courses
            course_num = int(input("Enter course number: "))
            course_name = str(input("Course Name (optional): "))
            if(course_name==""):
                # If course name is not provided, update Courses
                mycursor.execute(f"insert into Courses values (UUID(), {course_num});")
                mydb.commit()
                print("Course added without a name")
            else:
                # If course name is provided, update both Courses and CoursesWithNames
                uid = str(uuid.uuid4())
                mycursor.execute(f"insert into Courses values ('{uid}', {course_num});")
                mycursor.execute(f"insert into CoursesWithNames values ('{uid}', '{course_name}');")
                mydb.commit()
                print("Course", course_name, "has been added")

        elif(choice==2):
            # Input Values for Subject
            subject_code = str(input("Enter subject code: "))
            subject_name = str(input("Enter subject name: "))
            subject_abb = str(input("Enter subject abbreviation: "))
            # Update Subjects with the values
            mycursor.execute(f"insert into Subjects values ('{subject_code}', '{subject_name}', '{subject_abb}');")
            mydb.commit()
            print("Subject successfully added")

        elif(choice==3):
            # Input base values needed to create a Course Offering
            course_off_uuid = str(uuid.uuid4())
            subject_code = str(input("Enter subject code: "))
            term_code = str(input("Enter term code: "))
            course_name = str(input("Enter course name: "))
            # Use course name to search for the course_uuid in CoursesWithNames
            mycursor.execute(f"select course_uuid from CoursesWithNames where name = '{course_name}';")
            course_uuid = str(mycursor.fetchall())
            # String formatting course_uuid
            course_uuid = course_uuid.replace("[", "")
            course_uuid = course_uuid.replace("]", "")
            course_uuid = course_uuid.replace("(", "")
            course_uuid = course_uuid.replace(")", "")
            course_uuid = course_uuid.replace(",", "")
            course_uuid = course_uuid[0:38]
            if(course_uuid != ""):
                # If the course exists, update CourseOfferings and Subjects with the values
                mycursor.execute(f"insert into CourseOfferings values ('{course_off_uuid}', {course_uuid}, {term_code});")
                # Check if a subject exists with the provided subject code
                mycursor.execute(f"select code from Subjects where code = '{subject_code}';")
                check_code = str(mycursor.fetchall())
                if(check_code!="[]"):
                    # If the subject exists, update SubjectMembership with the values
                    mycursor.execute(f"insert into SubjectMemberships values ('{subject_code}', '{course_off_uuid}');")
                    mydb.commit()
                    print("Subject Membership has been saved")
                else:
                    # If the subject does not exist, break
                    print("Subject with this code does not exist, please insert the subject first")
                    break
                print("Course offering has been saved")
            else:
                # If the course does not exist, break
                print("Course with this name does not exist, please insert the course first")
                break
            
            section_uuid = str(uuid.uuid4())
            schedule_uuid = str(uuid.uuid4())
            print("Please provide the schedule for this course offering: ")
            # Input values to create a schedule
            start_time = int(input("Enter the start time: "))
            end_time = int(input("Enter the end time: "))
            while(True):
                # Assert correct time format 
                if(start_time<-1 or start_time>1439 or end_time<-1 or end_time>1439):
                    print("Incorrect format detected. Please enter minutes after midnight (0-1439)")
                    start_time = int(input("Enter the start time: "))
                    end_time = int(input("Enter the end time: "))
                else:
                    break
            days = str(input("Enter days of week (m tu w th f sat sun): "))
            flags = ["'false'"]*7
            day = days.split()
            i = 0
            while(i<len(day)):
                if(day[i]=="m"):
                    flags[0]="'true'"
                elif(day[i]=="tu"):
                    flags[1]="'true'"
                elif(day[i]=="w"):
                    flags[2]="'true'"
                elif(day[i]=="th"):
                    flags[3]="'true'"
                elif(day[i]=="f"):
                    flags[4]="'true'"
                elif(day[i]=="sat"):
                    flags[5]="'true'"
                elif(day[i]=="sun"):
                    flags[6]="'true'"
                else:
                    print(day[i] ,"is not recognized as a day and has been igonored")
                i=i+1
            # Update Schedules with the values provided by the user
            mycursor.execute(f"insert into Schedules values ('{schedule_uuid}', {start_time}, {end_time}, {flags[0]}, {flags[1]}, {flags[2]}, {flags[3]}, {flags[4]}, {flags[5]}, {flags[6]});")
            mydb.commit()
            print("Schedule has been saved")

            flag = str(input("Would you like to add a section to this course offering (y/n): "))
            section_num=-1
            if(flag=='y'):
                # If want to add section, input values
                section_num = int(input("Enter section number: "))
                section_type = str(input("Enter section type: "))
                # Update Sections with the values
                mycursor.execute(f"insert into Sections values ('{section_uuid}', '{course_off_uuid}', '{section_type}', {section_num}, '{schedule_uuid}');")
                mydb.commit()
                print("Section has been successfully assigned")
            else:
                # If do not want to add section, skip
                pass

            if(flag=='y'):
                check_room = str(input("Do you want to add a room for this section (y/n): "))
                if(check_room=="y"):
                    # If want to add a Room to the section, input values
                    room_code = str(input("Enter room code: "))
                    # Use room code to search for the room uuid
                    mycursor.execute(f"select room_uuid from RoomCodes where room_code = '{room_code}';")
                    room_uuid = str(mycursor.fetchall())
                    # String format room uuid
                    room_uuid = room_uuid.replace("[", "")
                    room_uuid = room_uuid.replace("]", "")
                    room_uuid = room_uuid.replace("(", "")
                    room_uuid = room_uuid.replace(")", "")
                    room_uuid = room_uuid.replace(",", "")
                    room_uuid = room_uuid[0:38]
                    if(room_uuid != ""):
                        # If the room exists, update SectionsWithRooms
                        mycursor.execute(f"insert into SectionsWithRooms values ('{section_uuid}', {room_uuid});")
                        mydb.commit()
                        print("Room succesfully added to this section")
                    else:
                        # If room does not exists show warning and do nothing
                        print("Room with this code does not exist, please insert the room first")
                else:
                    # Do not want to add a room - do nothing
                    pass

                check_teaching = str(input("Would you like to add a Teaching for this section (y/n): "))
                if(check_teaching=="y"):
                    # If want to add a Teaching, input values
                    instr_name = str(input("Enter instructors name: "))
                    # Use instructors name to search for instructor id in InstructorWithNames
                    mycursor.execute(f"select instructor_id from InstructorsWithNames where name = '{instr_name}';")
                    instr_id = str(mycursor.fetchall())
                    # String format instructor id
                    instr_id = instr_id.replace("[", "")
                    instr_id = instr_id.replace("]", "")
                    instr_id = instr_id.replace("(", "")
                    instr_id = instr_id.replace(")", "")
                    instr_id = instr_id.replace(",", "")
                    instr_id = instr_id.replace("Decimal", "")
                    instr_id = instr_id.replace("\"", "")
                    instr_id = instr_id.replace("'", "")
                    if(instr_id!=""):
                        # If instructor with the name provided exists, update Teachings
                        instr_id = int(instr_id)
                        mycursor.execute(f"insert into Teachings values ({instr_id}, '{section_uuid}');")
                        mydb.commit()
                        print("Teaching has been successfully saved")
                    else:
                        # If the instructor does not exist, show warning and do nothing
                        print("The instructor name does not exist, please insert the instructor first.")
                else:
                    # Do not want to add a Teaching - do nothing
                    pass

                check_gd = str(input("Would you like to add a grade distribution for this course offering (y/n): "))
                if(check_gd=='y'):
                    # If want to add Grade Distribution, input values
                    a_count = int(input("Enter number of A: "))
                    ab_count = int(input("Enter number of A-: "))
                    b_count = int(input("Enter number of B: "))
                    bc_count = int(input("Enter number of B-: "))
                    c_count = int(input("Enter number of C: "))
                    d_count = int(input("Enter number of D: "))
                    f_count = int(input("Enter number of F: "))
                    s_count = int(input("Enter number of S: "))
                    u_count = int(input("Enter number of U: "))
                    cr_count = int(input("Enter number of CR: "))
                    n_count = int(input("Enter number of N: "))
                    p_count = int(input("Enter number of P: "))
                    i_count = int(input("Enter number of I: "))
                    nw_count = int(input("Enter number of NW: "))
                    nr_count = int(input("Enter number of NR: "))
                    other_count = int(input("Enter number of other: "))
                    # Update GradeDistributions with the values
                    mycursor.execute(f"insert into GradeDistributions values ('{course_off_uuid}', {section_num}, {a_count}, {ab_count}, {b_count}, {bc_count}, {c_count}, {d_count}, {f_count}, {s_count}, {u_count}, {cr_count}, {n_count}, {p_count}, {i_count}, {nw_count}, {nr_count}, {other_count});")
                    mydb.commit()
                    print("Grade Distribution has been successfully saved")
                else:
                    # Do not want to add Grade Distribution - do nothing
                    pass
            else:
                # Do not want to add a section - do nothing
                pass
            
        elif(choice==4):
            # Input values for the room and facility
            uid = str(uuid.uuid4())
            room_code = str(input("Enter room code: "))
            facility_code = str(input("Enter facility code: "))
            # Update RoomFacilities and RoomCodes with the values
            mycursor.execute(f"insert into RoomFacilities values ('{uid}', '{facility_code}');")
            mycursor.execute(f"insert into RoomCodes values ('{uid}', '{room_code}');")
            mydb.commit()
            print("Room", room_code, "has been added to facility", facility_code)

        elif(choice==5):
            # Input values for instructor
            instr_id = int(input("Enter instructors id: "))
            instr_name = str(input("Enter instructors name (optional): "))
            # Update Instructors with the instructor id
            mycursor.execute(f"insert into Instructors values ({instr_id});")
            if(instr_name != ""):
                # If the name was provided, update InstructorsWithNames with name and id
                mycursor.execute(f"insert into InstructorsWithNames values ({instr_id}, '{instr_name}');")
                print("Instructor", instr_name, "has been added with id", instr_id)
            else:
                # If the name was not provided, do nothing
                print("Instructor id", instr_id ,"successfully added")
            mydb.commit()

        elif(choice==6):
            # Input base values to create a section
            section_uuid = str(uuid.uuid4())
            schedule_uuid = str(uuid.uuid4())
            section_num = int(input("Enter section number: "))
            section_type = str(input("Enter section type: "))
            course_name = str(input("Enter course name: "))
            term_code = int(input("Enter term code: "))
            # Using course name search for course uuid in CoursesWithNames
            mycursor.execute(f"select course_uuid from CoursesWithNames where name = '{course_name}';")
            course_uuid = str(mycursor.fetchall())
            # String format course uuid
            course_uuid = course_uuid.replace("[", "")
            course_uuid = course_uuid.replace("]", "")
            course_uuid = course_uuid.replace("(", "")
            course_uuid = course_uuid.replace(")", "")
            course_uuid = course_uuid.replace(",", "")
            course_uuid = course_uuid[0:38]
            if(course_uuid==""):
                # If the course does not exist, break
                print("Course with this name does not exist, please insert Course first")
                break
            # If the course exists, select course offering uuid using course uuid and term code from CourseOfferings
            mycursor.execute(f"select uuid from CourseOfferings where course_uuid = {course_uuid} and term_code = {term_code};")
            course_off_uuid = str(mycursor.fetchall())
            # String format course offering uuid
            course_off_uuid = course_off_uuid.replace("[", "")
            course_off_uuid = course_off_uuid.replace("]", "")
            course_off_uuid = course_off_uuid.replace("(", "")
            course_off_uuid = course_off_uuid.replace(")", "")
            course_off_uuid = course_off_uuid.replace(",", "")
            course_off_uuid = course_off_uuid[0:38]
            # Input values to create a Schedule
            print("Please provide the schedule for this course offering: ")
            start_time = int(input("Enter the start time: "))
            end_time = int(input("Enter the end time: "))
            while(True):
                # Assert correct time format 
                if(start_time<-1 or start_time>1439 or end_time<-1 or end_time>1439):
                    print("Incorrect format detected. Please enter minutes after midnight (0-1439)")
                    start_time = int(input("Enter the start time: "))
                    end_time = int(input("Enter the end time: "))
                else:
                    break
            days = str(input("Enter days of week (m tu w th f sat sun): "))
            flags = ["'false'"]*7
            day = days.split()
            i = 0
            while(i<len(day)):
                if(day[i]=="m"):
                    flags[0]="'true'"
                elif(day[i]=="tu"):
                    flags[1]="'true'"
                elif(day[i]=="w"):
                    flags[2]="'true'"
                elif(day[i]=="th"):
                    flags[3]="'true'"
                elif(day[i]=="f"):
                    flags[4]="'true'"
                elif(day[i]=="sat"):
                    flags[5]="'true'"
                elif(day[i]=="sun"):
                    flags[6]="'true'"
                else:
                    print(day[i] ,"is not recognized as a day and has been igonored")
                i=i+1
            # Update Schedules with the values provided by the user
            mycursor.execute(f"insert into Schedules values ('{schedule_uuid}', {start_time}, {end_time}, {flags[0]}, {flags[1]}, {flags[2]}, {flags[3]}, {flags[4]}, {flags[5]}, {flags[6]});")
            mydb.commit()
            print("Schedule has been saved")

            if(course_off_uuid != ""):
                # If the course offering exists, update Sections with the values
                mycursor.execute(f"insert into Sections values ('{section_uuid}', {course_off_uuid}, '{section_type}', {section_num}, '{schedule_uuid}');")
                mydb.commit()
                print("Section has been successfully assigned")
            else:
                # If the course offering does not exist, return error and break
                print("This CourseOffering does not exist, please insert the course offering first")
                break

            flag = str(input("Would you like to add a Teaching for this section (y/n): "))
            if(flag=="y"):
                # If want to add a Teaching, input values
                instr_name = str(input("Enter instructors name: "))
                # Use instructors name to search for instructor id in InstructorWithNames
                mycursor.execute(f"select instructor_id from InstructorsWithNames where name = '{instr_name}';")
                instr_id = str(mycursor.fetchall())
                # String format instructor id
                instr_id = instr_id.replace("[", "")
                instr_id = instr_id.replace("]", "")
                instr_id = instr_id.replace("(", "")
                instr_id = instr_id.replace(")", "")
                instr_id = instr_id.replace(",", "")
                instr_id = instr_id.replace("Decimal", "")
                instr_id = instr_id.replace("\"", "")
                instr_id = instr_id.replace("'", "")
                if(instr_id!=""):
                    # If instructor with the name provided exists, update Teachings
                    instr_id = int(instr_id)
                    mycursor.execute(f"insert into Teachings values ({instr_id}, '{section_uuid}');")
                    mydb.commit()
                    print("Teaching has been successfully saved")
                else:
                    # If instructor does not exist show warning and do nothing
                    print("The instructor name does not exist, please insert the instructor first.")
            else:
                # Do nothing
                pass

        elif(choice==7):
                # Return to the previous menu
                return
        else:
            raise AssertionError("Invalid choice")

def prompt_user_query():
    while(True):
        print("1. Search for a course schedule")
        print("2. Search for the room where the course section is offered")
        print("3. Search if your course is happening OFF CAMPUS or ONLINE")
        print("4. Search for all past course offerings of a course")
        print("5. Look up the average GPA of a course for a course offering")
        print("6. Search for the courses offered by an instructor")
        print("7. Which subjects has a course been offered under")
        print("8. Go Back")
        choice = int(input("Choose what to search/query: "))

        if(choice==1):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            term_code = str(input("Term Code: ")) 
            course_section_type = str(input("Course Section Type (LAB/LEC/DIS/FLD/IND/SEM): "))
            course_section_number = str(input("Section Number: "))
            
            mycursor.execute(f"WITH course_offerings AS (select uuid AS course_offering_uuid, name AS course_name, term_code from CoursesWithNames \
                            inner join CourseOfferings on CoursesWithNames.course_uuid=CourseOfferings.course_uuid), course_sections AS (select uuid as \
                            section_uuid, course_name, term_code, section_type, section_number, schedule_uuid from course_offerings inner join Sections on \
                            course_offerings.course_offering_uuid=Sections.course_offering_uuid), course_section_schedule AS (select course_name, term_code,\
                            section_type, section_number, start_time, end_time, mon, tues, wed, thurs, fri, sat, sun from course_sections inner join Schedules \
                            on course_sections.schedule_uuid=Schedules.uuid) select * from course_section_schedule where course_name='{course_name}' and term_code='{term_code}' \
                            and section_type='{course_section_type}' and section_number='{course_section_number}';")
            
            result = mycursor.fetchall()
            
            if(len(result)==0):
                print("\n")
                print("No schedule found")
            elif (int(result[0][4]) == -1 and result[0][5] == -1):
                print("\n")
                print("Schedule not available")
            else:
                print("\n")
                print("The following schedule has been found")
                start_time = int(result[0][4])
                end_time = int(result[0][5])
                start_hour = start_time // 60
                start_minutes = start_time % 60
                end_hour = end_time // 60
                end_minutes = end_time % 60
                mon, tues, wed, thurs, fri, sat, sun = result[0][6], result[0][7], result[0][8], result[0][9], result[0][10], result[0][11], result[0][12]
                mon, tues, wed, thurs, fri, sat, sun = bool(mon), bool(tues), bool(wed), bool(thurs), bool(fri), bool(sat), bool(sun)
                print("Course Name:", course_name)
                print("Section Type:", course_section_type + " Section Number:", course_section_number)
                
                if(start_minutes < 10):
                    print("Start Time: " + str(start_hour) + ":0" + str(start_minutes))
                else:
                    print("Start Time: " + str(start_hour) + ":" + str(start_minutes))

                if(end_minutes < 10):
                    print("End Time: " + str(end_hour) + ":0" + str(end_minutes))
                else:
                    print("End Time: " + str(end_hour) + ":" + str(end_minutes))

                print("Happens on following days: ")
                if(mon):
                    print("     Monday")
                if(tues):
                    print("     Tuesday")
                if(wed):
                    print("     Wednesday")
                if(thurs):
                    print("     Thursday")
                if(fri):
                    print("     Friday")
                if(sat):
                    print("     Saturday")
                if(sun):
                    print("     Sunday")
                print("\n")

        elif(choice==2):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("\n")
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            term_code = str(input("Term Code: "))
            course_section_type = str(input("Course Section Type (LAB/LEC/DIS/FLD/IND/SEM): "))
            course_section_number = str(input("Section Number: "))
            mycursor.execute(f"WITH course_offerings AS (select uuid AS course_offering_uuid, name AS course_name, term_code from CoursesWithNames \
                            inner join CourseOfferings on CoursesWithNames.course_uuid=CourseOfferings.course_uuid where name='{course_name}' and term_code='{term_code}' \
                            ), course_sections AS (select uuid as section_uuid, course_name, term_code, section_type, section_number from course_offerings inner join Sections on \
                            course_offerings.course_offering_uuid=Sections.course_offering_uuid where section_type='{course_section_type}' and section_number='{course_section_number}'), course_section_rooms AS ( \
                            select room_uuid, course_name, term_code, section_type, section_number from course_sections inner join SectionsWithRooms on course_sections.section_uuid=SectionsWithRooms.section_uuid \
                            ), course_section_room_number AS (select room_code, course_name, term_code, section_type, section_number from course_section_rooms inner join RoomCodes on course_section_rooms.room_uuid=RoomCodes.room_uuid \
                            ) select DISTINCT course_name, term_code, section_type, section_number, room_code from course_section_room_number;")
            result = mycursor.fetchall()
            
            if(len(result)==0):
                print("\n")
                print("No room found")
            else:
                print("\n")
                print("The following rooms has been found")
                for i in result:
                    print("Course Name: " + str(i[0]))
                    print("Term Code: " + str(i[1]))
                    print("Section Type: " + str(i[2]))
                    print("Section Number: " + str(i[3]))
                    print("Room Number: " + str(i[4]))
            print("\n")
            
        elif(choice==3):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("\n")
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            term_code = str(input("Term Code: "))

            mycursor.execute(f"WITH course_offerings AS (select uuid AS course_offering_uuid, name AS course_name, term_code from CoursesWithNames \
                            inner join CourseOfferings on CoursesWithNames.course_uuid=CourseOfferings.course_uuid where name='{course_name}' \
                            and term_code='{term_code}'), course_sections AS (select uuid as section_uuid, course_name, term_code, section_type, section_number \
                            from course_offerings inner join Sections on course_offerings.course_offering_uuid=Sections.course_offering_uuid), room_facilities \
                            AS (select course_name, term_code, room_uuid from course_sections inner join SectionsWithRooms on course_sections.section_uuid=SectionsWithRooms.section_uuid), \
                            facility_code AS (select course_name, term_code, facility_code from room_facilities inner join RoomFacilities on room_facilities.room_uuid=RoomFacilities.room_uuid \
                            ) select DISTINCT * from facility_code where facility_code='ONLINE' OR facility_code='OFF CAMPUS';")

            result = mycursor.fetchall()
            
            if(len(result)==0):
                print("\n")
                print("This course is not offered OFF-CAMPUS or ONLINE")
            else:
                print("\n")
                for i in result:
                    print("Course Name: " + str(i[0]))
                    print("Term Code: " + str(i[1]))
                    print("Facility Code: " + str(i[2]))
            print("\n")

        elif(choice==4):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("\n")
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            mycursor.execute(f"WITH course_query AS (SELECT * FROM CoursesWithNames where name='{course_name}') SELECT name,term_code \
                            from course_query inner join CourseOfferings on course_query.course_uuid = CourseOfferings.course_uuid;")
            
            result = mycursor.fetchall()
            
            if(result==[]):
                print("\n")
                print("No course with this name has been found")
            else:
                print("\n")
                print("The following course offerings (terms) have been found for this course: ")
                print("\n")
                for i in result:
                    print("Term: " + str(i[1]))
                print("\n")

        elif(choice==5):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            term_code = str(input("Term Code: "))
            mycursor.execute(f"WITH course_term_search AS (select uuid AS course_offering_uuid, name AS course_name, term_code from CoursesWithNames inner join \
                            CourseOfferings on CoursesWithNames.course_uuid=CourseOfferings.course_uuid where name='{course_name}' and term_code='{term_code}' \
                            ) SELECT course_term_search.course_offering_uuid, course_name, section_number, a_count, ab_count, b_count, bc_count, c_count, d_count,\
                            f_count from course_term_search inner join GradeDistributions on course_term_search.course_offering_uuid=GradeDistributions.course_offering_uuid;")
            
            result = mycursor.fetchall()
            if(len(result)==0):
                print("\n")
                print("No grade distribution found")
            else:
                gpa_count = 0
                gpa_sum = 0

                for i in result:
                    a_count = int(i[3])
                    ab_count = int(i[4])
                    b_count = int(i[5])
                    bc_count = int(i[6])
                    c_count = int(i[7])
                    d_count = int(i[8])
                    f_count = int(i[9])
                    total = a_count + ab_count + b_count + bc_count + c_count + d_count + f_count
                    total_gpa = a_count*4 + ab_count*3.5 + b_count*3 + bc_count*2.5 + c_count*2 + d_count*1 + f_count*0
                    average_gpa = float(total_gpa/total)
                    gpa_sum += average_gpa
                    gpa_count += 1

                print("\n")
                print("Average GPA for " + course_name + " offered in " + term_code + " is " + str(gpa_sum/gpa_count))
                print("\n")

        elif(choice==6):
            instructor_name = None
            while(True):
                instructor_name_query = str(input("Enter Instructor Name: "))
                mycursor.execute(f"SELECT * FROM InstructorsWithNames where name='{instructor_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("No instructor found with this name")
                else:
                    instructor_name = instructor_name_query
                    break
            
            mycursor.execute(f"WITH instructor_search AS (select instructor_id, name as instructor_name from InstructorsWithNames where name='{instructor_name}' \
                            ), teachings_instructor AS (select instructor_search.instructor_id, instructor_name, section_uuid from instructor_search inner join Teachings \
                            on instructor_search.instructor_id=Teachings.instructor_id), instructor_sections AS (select instructor_id, instructor_name, course_offering_uuid \
                            from teachings_instructor inner join Sections on teachings_instructor.section_uuid=Sections.uuid), course_offerings_instructor AS ( \
                            select instructor_id, instructor_name, course_uuid from instructor_sections inner join CourseOfferings on instructor_sections.course_offering_uuid=CourseOfferings.uuid \
                            ), courses_taught AS (select instructor_id, instructor_name, name as course_name from course_offerings_instructor inner join CoursesWithNames on \
                            course_offerings_instructor.course_uuid=CoursesWithNames.course_uuid) select DISTINCT instructor_name, course_name from courses_taught;")
            
            result = mycursor.fetchall()
            
            print("\n")
            if(len(result)==0):
                print("No courses taught by this instructor")
            else:
                for i in result:
                    print("Instructor Name: " + str(i[0]) + " Course Name: " + str(i[1]))
                print("\n")
                
        elif(choice==7):
            course_name = None
            while(True):
                course_name_query = str(input("Enter Course Name: "))
                mycursor.execute(f"select * from CoursesWithNames where name='{course_name_query}';")
                result = mycursor.fetchall()
                if(len(result)==0):
                    print("No course found with this name")
                else:
                    course_name = course_name_query
                    break
            
            mycursor.execute(f"WITH course_offerings_search AS (select uuid, name AS course_name from CoursesWithNames inner join CourseOfferings \
                            on CoursesWithNames.course_uuid=CourseOfferings.course_uuid where name='{course_name}'), course_subjects AS (select DISTINCT \
                            course_name, subject_code from course_offerings_search inner join SubjectMemberships on course_offerings_search.uuid=SubjectMemberships.course_offering_uuid \
                            ) SELECT course_name, subject_code, name as subject_name from course_subjects inner join Subjects on course_subjects.subject_code=Subjects.code;")
            result = mycursor.fetchall()
            
            if(result==[]):
                print("\n")
                print("No course with this name has been found")
            else:
                print("\n")
                print("The following subjects have been found for this course: ")
                print("\n")
                for i in result:
                    print("Course Name: " + str(i[0]) + " Subject Code: " + str(i[1]) + " Subject Name: " + str(i[2]))
                print("\n")
                
        elif(choice==8):
            return
        else:
            raise AssertionError("Invalid choice")

mydb = get_mydb()
mycursor = mydb.cursor()
prompt_user()